#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "graph.h"


int test_str_to_graph(char *in, char *out){
    Graph_T *GG = init_graph_from_string(in);
    int res = strncmp(graph_to_str(GG), out, strlen(Graph_To_str(GG)));
    if (res != 0){
        fprintf(stderr, "Error in test_str_to_graph. Output not as expected\n");
        return -1;
    }
    return 0;
}

int main(int argc, char *argv[]){
    if (argc != 3){
        fprintf(stderr, "Error, main in Graph_Test called with too few arguments\n.");
        return -1;
    }
    char *in = argv[1], *out = argv[2];
    if (test_str_to_graph(in, out) == -1){
        printf("test_str_to_graph : test failed\n.");
    }
    return 0;
}
