#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#

import sys
import subprocess
import glob


CC = "gcc"
SRC_DIR = "../src"
INCLUDE_DIR = "../include"
CC_FLAGS = ["-Wall", "-Wextra", "-Wshadow"]
OUT = "test_"


def compile_c_src(test_src, src_list, out):
    call_args = [CC]
    call_args += ["-o", "%s%s" % (OUT, out)]
    call_args += ["%s/%s" % (SRC_DIR, src_filename) for src_filename in src_list]
    call_args += test_src
    call_args += ["-I%s" % INCLUDE_DIR]
    call_args += CC_FLAGS
    return subprocess.check_output(call_args)


def exec_c(exec_filename, args):
    call_args = [exec_filename]
    call_args += args
    return subprocess.check_output(call_args)


def main():
    # test graph
    compile_c_src(["graph_test.c"], ["graph.c"], "graph")
    in_entries = glob.glob("test_graph_to_str_instances/*.in")
    for in_entry in in_entries:
        with open(in_entry) as f_in:
            str_in = f_in.read()
        with open(in_entry[:-2]+"out") as f_out:
            str_out = f_out.read()

        exec_output = exec_c("./test_graph", [str_in, str_out])
        if len(exec_output) > 0:
            print(exec_output)


if __name__ == "__main__":
    main()
