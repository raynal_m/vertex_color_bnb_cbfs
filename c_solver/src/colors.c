#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "graph.h"
#include "colors.h"


Colors_T *colors_init(int nb_vertices){
    Colors_T *C = malloc(sizeof(Colors_T));
    C->colors = malloc(nb_vertices * sizeof(int));
    C->nb_colors_used = C->nb_vertices_colored = 0;
    C->lower_bound = 0;
    C->upper_bound = INFINITE;
    return C;
}


void colors_free(Colors_T *C){
    free(C->colors);
    free(C);
}


void colors_color_vertex(Colors_T *C, int u, int c){
    C->colors[u] = c;
    if (C->nb_colors_used < c){
        C->nb_colors_used = c;
    }
    C->nb_vertices_colored ++;
}


Colors_T *colors_copy(Colors_T *C){
    Colors_T *new_C = malloc(sizeof(Colors_T));
    new_C->colors = malloc(G->nb_vertices * sizeof(int));
    memcpy((void *)new_C->colors, (void *)C->colors, G->nb_vertices * sizeof(int));
    new_C->nb_colors_used = C->nb_colors_used;
    new_C->nb_vertices_colored = C->nb_vertices_colored;
    new_C->lower_bound = C->lower_bound;
    new_C->upper_bound = C->upper_bound;
    return new_C;
}


int colors_get_dsat(Colors_T *C, int u){
    int result = 0;
    for (int v = 0; v < G->nb_vertices; v++){
        if ((G->adj_matrix[u][v] == EXISTS_EDGE) && (C->colors[v] != UNCOLORED)){
            result ++;
        }
    }
    return result;
}

int get_max_dsat_vertex(Colors_T *C){
    int result = 0, max_dsat = 0, current_dsat;
    for (int u = 0; u < G->nb_vertices; u++){
        current_dsat = colors_get_dsat(C, u);
        if (current_dsat > max_dsat){
            result = u;
            max_dsat = current_dsat;
        }
    }
    return result;
}

int colors_get_uncolored_degree(Colors_T *C, int u){
    int result = 0;
    for (int v = 0; v < G->nb_vertices; v++){
        if ((G->adj_matrix[u][v] == EXISTS_EDGE) && (C->colors[v] == UNCOLORED)){
            result ++;
        }
    }
    return result;
}

int get_smallest_available_color(Colors_T *C, int u){
    bool is_available_color;
    for (int c = 1; ; c++){
        is_available_color = true;
        for (int v = 0; v < G->nb_vertices; v++){
            if ((G->adj_matrix[u][v] == EXISTS_EDGE) && (C->colors[v] == c)){
                is_available_color = false;
                break;
            }
        }
        if (is_available_color){
            return c;
        }
    }
}


bool colors_is_possible_color(Colors_T *C, int u, int c){
    for (int v = 0; v < G->nb_vertices; v++){
        if ((G->adj_matrix[u][v] == EXISTS_EDGE) && (C->colors[v] == c)){
            return false;
        }
    }
    return true;
}

bool colors_is_valid_coloring(Colors_T *C){
    colors_print_dbg(C);
    for (int u = 0; u < G->nb_vertices; u++){
        if ((C->colors[u] < 1) || (C->colors[u] > C->nb_colors_used)){
            return false;
        }
        for (int v = 0; v < G->nb_vertices; v++){
            if ((G->adj_matrix[u][v] == EXISTS_EDGE) && (C->colors[u] == C->colors[v])){
                return false;
            }
        }
    }
    return true;
}

void colors_print_dbg(Colors_T *C){
    printf("** nb_v_col = %d ; nb_col_use = %d ; lb = %d, ub = %d", C->nb_vertices_colored, C->nb_colors_used, C->lower_bound, C->upper_bound);
    for (int i = 0; i < G->nb_vertices; i++){
        printf("[%d->%d]", i, C->colors[i]);
    }
    printf("\n");
}
