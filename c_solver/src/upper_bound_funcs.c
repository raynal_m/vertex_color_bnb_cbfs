#include <assert.h>

#include "colors.h"
#include "graph.h"
#include "upper_bound_funcs.h"


UpperBoundFunc_T *get_upper_bound_func(int index){
    switch(index){
    case 0:
        return get_upper_bound_0;
    default:
        assert(0 && "Error ! unknown upper_bound_func index\n");
    }
}



int get_upper_bound_0(Colors_T *C){
    Colors_T *C_copy = colors_copy(C);
    int u, c, result;
    while(C_copy->nb_vertices_colored < G->nb_vertices){
        u = get_max_dsat_vertex(C_copy);
        c = get_smallest_available_color(C_copy, u);
        colors_color_vertex(C_copy, u, c);
    }
    result = C_copy->nb_colors_used;
    colors_free(C_copy);
    return result;
}
