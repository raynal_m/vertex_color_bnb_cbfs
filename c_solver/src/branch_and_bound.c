#include <stdlib.h>
#include <assert.h>

#include "graph.h"
#include "colors.h"
#include "cbfs_heap.h"
#include "lower_bound_funcs.h"
#include "upper_bound_funcs.h"
#include "heap_cmp_funcs.h"
#include "branching_vertex_choice_funcs.h"


int vertex_coloring_branch_and_bound(
                                     HeapCmpFunc_T **heap_cmp_funcs,
                                     LowerBoundFunc_T *get_lower_bound,
                                     UpperBoundFunc_T *get_upper_bound,
                                     BranchingVertexChoiceFunc_T *choose_branching_vertex){
    CbfsHeap_T *Q = cbfs_heap_init(G->nb_vertices+1, heap_cmp_funcs);
    Colors_T *C_init = colors_init(G->nb_vertices);
    C_init->lower_bound = get_lower_bound(C_init);
    cbfs_heap_push(Q, C_init, 0);

    int B = INFINITE;
    int u, c;
    Colors_T *C, *new_C, *best_C = NULL;

    while(!cbfs_heap_is_empty(Q)){
        C = cbfs_heap_pop(Q);
        C->upper_bound = get_upper_bound(C);

        u = choose_branching_vertex(C);
        if (C->nb_vertices_colored == G->nb_vertices){
            if (C->upper_bound < B){
                B = C->upper_bound;
                best_C = colors_copy(C);
            }
        } else {
            for (c = 1; c <= C->nb_colors_used; c++){
                if (colors_is_possible_color(C, u, c)){
                    new_C = colors_copy(C);
                    colors_color_vertex(new_C, u, c);
                    new_C->lower_bound = get_lower_bound(new_C);
                    if (new_C->lower_bound <= B){
                        cbfs_heap_push(Q, new_C, new_C->nb_colors_used);
                    }
                }
            }
            colors_color_vertex(C, u, c);
            C->lower_bound = get_lower_bound(C);
            if (C->lower_bound <= B){
                cbfs_heap_push(Q, C, C->nb_colors_used);
            }
        }
    }
    assert(colors_is_valid_coloring(best_C) &&
           "Error ! vertex_coloring_branch_and_bound does not return a valid coloring!\n");
    return B;
}
