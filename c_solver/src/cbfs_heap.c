#include <stdlib.h>

#include "cbfs_heap.h"


Heap_T *heap_init(HeapCmpFunc_T *compar){
    Heap_T *heap = malloc(sizeof(Heap_T));
    heap->nodes = malloc(HEAP_INIT_SIZE * sizeof(HeapData_T));
    heap->alloc_size = HEAP_INIT_SIZE;
    heap->compar = compar;
    heap->nb_elem = 0;
    return heap;
}


void heap_free(Heap_T *heap){
    free(heap->nodes);
    free(heap);
}


void heap_push(Heap_T *heap,  HeapData_T data){
    heap->nb_elem ++;
    if (heap->nb_elem == heap->alloc_size){
        heap->nodes = realloc((void *)(heap->nodes),
                              2*heap->alloc_size*sizeof(HeapData_T *));
        heap->alloc_size *= 2;
    }
    int i = heap->nb_elem;
    while (i > 1){
        if (!heap->compar(data, heap->nodes[i/2])){
            break;
        }
        heap->nodes[i] = heap->nodes[i/2];
        i /= 2;
    }
    heap->nodes[i] = data;
}


HeapData_T heap_pop(Heap_T *heap){
    if (heap->nb_elem == 0){
        return NULL;
    }
    HeapData_T data = heap->nodes[1];
    int i = 1, j;
    while (1){
        if (2*i < heap->nb_elem){
            j = (heap->compar(heap->nodes[2*i], heap->nodes[2*i+1]) ?
                 2*i :
                 2*i+1);
            if (heap->compar(heap->nodes[heap->nb_elem], heap->nodes[j])){
                break;
            }
        } else if (2*i > heap->nb_elem) {
            break;
        } else if (!heap->compar(heap->nodes[heap->nb_elem], heap->nodes[2*i])){
            j = 2*i;
        } else {
            break;
        }
        heap->nodes[i] = heap->nodes[j];
        i = j;
    }
    heap->nodes[i] = heap->nodes[heap->nb_elem];
    heap->nb_elem --;
    return data;
}


CbfsHeap_T *cbfs_heap_init(int nb_heaps, HeapCmpFunc_T **compar_funcs){
    CbfsHeap_T *cbfs_heap = malloc(sizeof(CbfsHeap_T));
    cbfs_heap->nb_heaps = nb_heaps;
    cbfs_heap->pop_index = cbfs_heap->nb_elem = 0;
    cbfs_heap->heaps = malloc(nb_heaps * sizeof(Heap_T));
    for (int i = 0; i < nb_heaps; i++){
        cbfs_heap->heaps[i] = heap_init(compar_funcs[i]);
    }
    return cbfs_heap;
}


void cbfs_heap_free(CbfsHeap_T *cbfs_heap){
    for (int i = 0; i < cbfs_heap->nb_heaps; i++){
        heap_free(cbfs_heap->heaps[i]);
    }
    free(cbfs_heap);
}

bool cbfs_heap_is_empty(CbfsHeap_T *cbfs_heap){
    return cbfs_heap->nb_elem == 0;
}


void cbfs_heap_push(CbfsHeap_T *cbfs_heap, HeapData_T data, int push_index){
    heap_push(cbfs_heap->heaps[push_index], data);
    cbfs_heap->nb_elem ++;
}

HeapData_T cbfs_heap_pop(CbfsHeap_T *cbfs_heap){
    if (cbfs_heap->nb_elem == 0){
        return NULL;
    }
    Heap_T *heap = cbfs_heap->heaps[cbfs_heap->pop_index];
    while (heap->nb_elem == 0){
        cbfs_heap->pop_index = (cbfs_heap->pop_index + 1) % cbfs_heap->nb_heaps;
        heap = cbfs_heap->heaps[cbfs_heap->pop_index];
    }
    cbfs_heap->pop_index = (cbfs_heap->pop_index + 1) % cbfs_heap->nb_heaps;
    cbfs_heap->nb_elem --;
    return heap_pop(heap);
}
