#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>


#include "graph.h"
#include "colors.h"
#include "heap_cmp_funcs.h"


HeapCmpFunc_T **get_heap_cmp_funcs(int index){
    HeapCmpFunc_T **heap_cmp_funcs = malloc((G->nb_vertices+1) * sizeof(HeapCmpFunc_T *));
    for (int i = 0; i <= G->nb_vertices; i++){
        switch(index){
        case 0:
            heap_cmp_funcs[i] = cbfs_heap_cmp_func_0;
            break;
        default:
            assert(0 && "Error ! unknown heap_cmp_func index\n");
        }
    }
    return heap_cmp_funcs;
}

bool cbfs_heap_cmp_func_0(Colors_T *C1, Colors_T *C2){
    return C1->nb_colors_used < C2->nb_colors_used;
}
