#ifndef LOWER_BOUND_FUNCS_DOT_H
#define LOWER_BOUND_FUNCS_DOT_H

#include "colors.h"


typedef int LowerBoundFunc_T(Colors_T *);

LowerBoundFunc_T *get_lower_bound_func(int index);

int get_lower_bound_0(Colors_T *C);

#endif
