#ifndef CBFS_HEAP_DOT_H
#define CBFS_HEAP_DOT_H

#include <stdlib.h>
#include <stdbool.h>

#include "colors.h"

#define HEAP_INIT_SIZE 16

typedef Colors_T * HeapData_T;

typedef bool HeapCmpFunc_T(HeapData_T, HeapData_T);

typedef struct{
    HeapData_T *nodes;
    HeapCmpFunc_T *compar;
    int alloc_size, nb_elem;
} Heap_T;


typedef struct{
    int nb_heaps;
    Heap_T **heaps;
    int pop_index;
    int nb_elem;
} CbfsHeap_T;

Heap_T *heap_init(HeapCmpFunc_T *compar);

void heap_free(Heap_T *heap);

void heap_push(Heap_T *heap, HeapData_T data);

HeapData_T heap_pop(Heap_T *heap);

CbfsHeap_T *cbfs_heap_init(int nb_heaps, HeapCmpFunc_T **compar_funcs);

void cbfs_heap_free(CbfsHeap_T *cbfs_heap);

bool cbfs_heap_is_empty(CbfsHeap_T *cbfs_heap);

void cbfs_heap_push(CbfsHeap_T *cbfs_heap, HeapData_T data, int index);

HeapData_T cbfs_heap_pop(CbfsHeap_T *cbfs_heap);

#endif
