#ifndef UPPER_BOUND_FUNCS_DOT_H
#define UPPER_BOUND_FUNCS_DOT_H

#include "colors.h"

typedef int UpperBoundFunc_T(Colors_T *);

UpperBoundFunc_T *get_upper_bound_func(int index);

int get_upper_bound_0(Colors_T *C);


#endif
