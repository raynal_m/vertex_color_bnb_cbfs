#ifndef BNB_COLORS_DOT_H
#define BNB_COLORS_DOT_H

#include <stdlib.h>
#include <stdbool.h>

#define INFINITE 20000

#define UNCOLORED 0

typedef struct{
    int *colors;
    int nb_colors_used;
    int nb_vertices_colored;
    int lower_bound;
    int upper_bound;
} Colors_T;


Colors_T *colors_init(int nb_vertices);

void colors_free(Colors_T *C);

void colors_color_vertex(Colors_T *C, int u, int c);

Colors_T *colors_copy(Colors_T *C);

int colors_get_dsat(Colors_T *C, int u);

int get_max_dsat_vertex(Colors_T *C);

int colors_get_uncolored_degree(Colors_T *C, int u);

int get_smallest_available_color(Colors_T *C, int u);

bool colors_is_possible_color(Colors_T *C, int u, int c);

bool colors_is_valid_coloring(Colors_T *C);

void colors_print_dbg(Colors_T *C);

#endif
