#ifndef HEAP_CMP_FUNCS_DOT_H
#define HEAP_CMP_FUNCS_DOT_H

#include <stdbool.h>

#include "cbfs_heap.h"
#include "colors.h"
#include "graph.h"


HeapCmpFunc_T **get_heap_cmp_funcs(int index);

bool cbfs_heap_cmp_func_0(Colors_T *C1, Colors_T *C2);


#endif
