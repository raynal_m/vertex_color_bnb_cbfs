#ifndef BRANCHING_VERTEX_CHOICE_FUNCS_DOT_H
#define BRANCHING_VERTEX_CHOICE_FUNCS_DOT_H

#include "graph.h"
#include "colors.h"


typedef int BranchingVertexChoiceFunc_T(Colors_T *);

BranchingVertexChoiceFunc_T *get_branching_vertex_choice_func(int index);

int branching_vertex_choice_func_0(Colors_T *C);


#endif
