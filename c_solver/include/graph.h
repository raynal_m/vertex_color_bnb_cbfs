#ifndef V_COLOR_GRAPH_DOT_H
#define V_COLOR_GRAPH_DOT_H

#include <stdlib.h>
#include <stdio.h>

#define EXISTS_EDGE 1
#define NO_EDGE 0


typedef struct{
    int nb_vertices;
    int **adj_matrix;
} Graph_T;

extern Graph_T *G;


Graph_T *graph_init(int nb_vertices);

void graph_add_edge(Graph_T *GG, int u, int v);

int graph_get_degree(Graph_T *GG, int u);

Graph_T *init_graph_from_string(char *graph_as_str);

Graph_T *init_graph_from_file(FILE *file);

char *graph_to_str(Graph_T *GG);


#endif
