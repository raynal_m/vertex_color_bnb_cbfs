#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#

import os
import subprocess
import glob

CC = "gcc"
SRC_DIR = "src"
INCLUDE_DIR = "include"
CC_FLAGS = ["-Wall", "-Wextra", "-Wshadow", "-O2"]
BIN_DIR = "bin"
BIN_FILENAME = "vertex_coloring_bnb"



def compile_c_src():
    if not os.path.isdir(BIN_DIR):
        os.mkdir(BIN_DIR)
    call_args = [CC]
    call_args += ["-o", "%s/%s" % (BIN_DIR, BIN_FILENAME)]
    call_args += glob.glob("%s/*.c" % SRC_DIR)
    call_args += ["-I%s" % INCLUDE_DIR]
    call_args += CC_FLAGS
    print("Compiling C files ...")
    print(" ".join(call_args))
    return subprocess.check_output(call_args)

if __name__ == "__main__":
    sstr = compile_c_src().decode('utf-8')
    if sstr is not "":
        print(sstr)
