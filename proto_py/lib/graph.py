#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from random import choice, randrange

class Graph():
    def __init__(self):
        self.m_id = 0
        self.m_adjacencies = dict()

    def add_vertex(self):
        new_vertex = self.m_id
        self.m_id += 1
        self.m_adjacencies[new_vertex] = set()
        return new_vertex

    def add_edge(self, u, v):
        assert(u in self.m_adjacencies and v in self.m_adjacencies)
        self.m_adjacencies[u].add(v)
        self.m_adjacencies[v].add(u)

    def vertices(self):
        return range(self.m_id)

    def nb_vertices(self):
        return self.m_id

    def adj(self, u):
        return self.m_adjacencies[u]

    def degree(self, u):
        return len(self.m_adjacencies[u])

    def dsat(self, u, colors):
        colors_seen = set()
        for v in self.m_adjacencies[u]:
            if v in colors and colors[v] not in colors_seen:
                colors_seen.add(v)
        return len(colors_seen)


def gen_random_connected_graph(n ,e):
    # get a reasonable number of edges
    if e < n-1 or e > n*(n-1)/2:
        e = (n*(n-1))//3
    g = Graph()
    for i in range(n):
        g.add_vertex()
    to_connect = {i for i in range(n)}
    connected = set()
    v = choice(tuple(to_connect))
    to_connect.remove(v)
    connected.add(v)

    while to_connect: # is not empty
        u = choice(tuple(to_connect))
        v = choice(tuple(connected))
        g.add_edge(u, v)
        to_connect.remove(u)
        connected.add(u)

    to_add = e-n+1
    while to_add > 0:
        u, v = randrange(n), randrange(n)
        if u != v and u not in g.adj(v):
            g.add_edge(u, v)
            to_add -= 1

    return g


def to_adj_matrix(g):
    return [
        [1 if v in g.adj(u) else 0 for v in range(g.nb_vertices())]
        for u in range(g.nb_vertices())
    ]
